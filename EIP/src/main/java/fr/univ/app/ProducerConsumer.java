package fr.univ.app;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.log4j.BasicConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import tp.model.Animal;
import tp.model.Center;
import tp.model.*;

public class ProducerConsumer {
	static String position = "";
	public static void main(String[] args) throws Exception {

		// using Spring Context
		final ApplicationContext springContext = new AnnotationConfigApplicationContext(fr.univ.config.AppConfig.class);
		// Configure le logger par défaut
		BasicConfigurator.configure();
		// Contexte Camel par défaut
		CamelContext context = new DefaultCamelContext();
		// Crée une route contenant le consommateur
		RouteBuilder routeBuilder = new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				// On définit un consommateur 'consumer-1'
				// qui va écrire le message
				from("direct:consumer-1").to("log:afficher-consumer-1");
				from("direct:route-1").choice().when(header("entete1").isEqualTo("toto")).to("direct:toto")
						.when(header("entete2").isEqualTo("titi")).to("direct:titi").otherwise().to("direct:tutu");
				from("direct:toto").to("log:afficher-toto");
				from("direct:titi").to("log:afficher-titi");
				from("direct:tutu").to("log:afficher-tutu");
				// definition de second consumer
				from("direct:messages-consumer-2").to("file:messages-consumer-2");
				// definition du nouvelle route
				from("direct:consumer-all").choice().when(header("entete").isEqualTo("ecrire"))
						.to("direct:messages-consumer-2").otherwise().to("direct:consumer-1");

				// GETTING ANIMAL INFORMATIQUE BY-NAME REQUEST
				from("direct:Citymanager").setHeader(Exchange.HTTP_METHOD, constant("GET"))
						.setHeader(Exchange.HTTP_PATH).simple("/rest-service/zoo-manager/find/byName/${headers.name}")
						.to("http://localhost:8080").log("response received : ${body}");

				// SENDING REQUEST TO THE GEONAME WEB-SERVICES
				from("direct:Geoname").setHeader(Exchange.HTTP_METHOD, constant("GET")).setHeader(Exchange.HTTP_PATH)
						.simple("/search?q=${headers.pays}&maxRows=10&style=LONG&lang=es&username=abdelkader").to("http://api.geonames.org").process(new Processor() {

							public void process(Exchange exchange) throws Exception {
								// TODO Auto-generated method stub
								String res = exchange.getIn().getBody(String.class);
								System.out.println(res);
							}
						});
				
				from("direct:Position").setHeader(Exchange.HTTP_METHOD, constant("GET")).setHeader(Exchange.HTTP_PATH)
				.simple("/rest-service/zoo-manager/animals/${headers.id}/position").to("http://localhost:7791").process(new Processor() {

					public void process(Exchange exchange) throws Exception {
						// TODO Auto-generated method stub
						position = exchange.getIn().getBody(String.class);
						System.out.println(position);
						System.out.println(position.indexOf("name"));
						String pays = position.substring(position.indexOf("name") + 5);
						int indexOfFirstChevron = pays.indexOf("<");
						System.out.println(indexOfFirstChevron);
						pays = pays.substring(0, indexOfFirstChevron);
						//System.out.println(position.substring(position.indexOf("name") + , position.indexOf("name") + 3));
						System.out.println(pays);
						ProducerTemplate pt1 = context.createProducerTemplate();
						//pt1.setDefaultEndpointUri("direct:Geoname");
						pt1.sendBodyAndHeader("direct:Geoname", "g", "pays", pays);
						
					}
				});

				// QUESTION 4 : REQUEST TO 3 INSTANCES TOMCAT
				from("direct:instance1").setHeader(Exchange.HTTP_METHOD, constant("GET")).setHeader(Exchange.HTTP_PATH)
						.simple("/rest-service/zoo-manager/animals").to("http://localhost:8181").to("direct:start");
				from("direct:instance2").setHeader(Exchange.HTTP_METHOD, constant("GET")).setHeader(Exchange.HTTP_PATH)
						.simple("/rest-service/zoo-manager/animals").to("http://localhost:8282")
						.to("direct:start");
				from("direct:instance3").setHeader(Exchange.HTTP_METHOD, constant("GET")).setHeader(Exchange.HTTP_PATH)
						.simple("/rest-service/zoo-manager/animals").to("http://localhost:8383")
						.to("direct:start");

				// JOINING MESSAGE WITH AGGREGATE EIP AGGREGATE PATTERN
				from("direct:start").log("Sending ${body} with correlation key ${header.myId}")
						.aggregate(header("myId"), springContext.getBean(AggregationStrategy.class)).completionSize(3)
						.log("Sending out ${body}").to("mock:result");
			}
		};
		// On ajoute la route au contexte
		routeBuilder.addRoutesToCamelContext(context);
		// On démarre le contexte pour activer les routes
		context.start();
		// On crée un producteur
		ProducerTemplate pt = context.createProducerTemplate();
		// qui envoie un message au consommateur 'consumer-1'
		// pt.sendBody("direct:consumer-1", "Hello world !");
		Scanner scanner = new Scanner(System.in);
		String message = new String("");
		while (!message.equals("exit")) {
			if (message.equals("exit"))
				System.out.println("connection down");
			message = scanner.nextLine();
			if (message.startsWith("w")) {
				pt.sendBodyAndHeader("direct:consumer-all", message, "entete", "ecrire");
			} else if (message.startsWith("byName")) {

				pt.sendBodyAndHeader("direct:Citymanager1", message, "name", "Tic");
				// pt.setDefaultEndpointUri("direct:Citymanager");
				//
				// pt.sendBody("direct:Citymanager");
				// pt.sendbody
			} else if (message.startsWith("instance")) {

				pt.asyncRequestBodyAndHeader("direct:instance1", message, "myId", 1);
				pt.asyncRequestBodyAndHeader("direct:instance2", message, "myId", 1);
				pt.asyncRequestBodyAndHeader("direct:instance3", message, "myId", 1);
				// pt.setDefaultEndpointUri("direct:Citymanager");
				//
				// pt.sendBody("direct:Citymanager");
				// pt.sendbody
			} else if (message.startsWith("g")) {
				// pt.sendBodyAndHeader("direct:Geoname", message, "q","london");
				Map<String, Object> urlVariables = new HashMap<String, Object>();
				urlVariables.put("q", "london");
				urlVariables.put("maxRows", "10");
				urlVariables.put("username", "abdelkader");
				pt.setDefaultEndpointUri("direct:Position");
				pt.sendBodyAndHeader("direct:Position", message, "id","8262cb5b-84de-4acf-85bf-649579959513");
				//pt.sendBody("direct:Geoname");

			} else if (message.startsWith("a")) {
				pt.asyncRequestBodyAndHeader("direct:start", "A", "myId", 1);
				pt.asyncRequestBodyAndHeader("direct:start", "B", "myId", 1);
				pt.asyncRequestBodyAndHeader("direct:start", "F", "myId", 2);
				pt.asyncRequestBodyAndHeader("direct:start", "C", "myId", 1);
			}

		}

	}
}
